# Machine Learning Making computers think

## Understand consept of Machine learning ##

Consept:

E(xperience) * T(ask)  = P(erformance)

Experience:
- Data input: housing prices, customer transactions, clickstream data, images

Task:
- Task: Predicts prices, segment coustomers, optimize user flows, categorize images

Perfomance:
- Accurace prices, coherent groupings, KPI lifts, correclty sorted images

Machine learning is the field of comupter science that gives computer the ability to learn without being explicitly programmed.


### Setting up environment ###

Install:
- Python 3
- pip install virtualenv
- virtualenv nlenv
- cd nlenv/script
- activate

### Install dependencies ###

Install using -  pip install:
- sklearn
- pandas
- numpy
- scipy

or create requirements.txt

```
sklearn
pandas
numpy
scipy
```
and run pip install -r requirements.txt

pip install jupyter

jupyter notebook

### Data Fetching ###

use kaggle Online dataset Database or use google dataset search. Rest API's / Web Api or OMDb API(the open movie database) or Weather APIs

Scraping / Crowling

### Section 03 ###

Introduction - outline

- Data exploration
- Numeric vs Categorical data
- Descriptive statistics / Inferential statics
- Data visualization
- Handling outliers

packages / liraries required

- scikit learn
- matplotlib
- numpy
- seaborn

### Section 04 ###

Outline
- Standardization
- Normalization
- Handling categorical data
- Handling missing values

### Section 05 ###

Outline
- Different types of Models
- Preparing data for modelling
- Which model to use
- Feature selection

Types of models

- Tree-based models
- Non-tree based models

Types of Algorithms

- Desicion tree
- Support Vector machine (SVM)
- Naive Bayes P (A|B) = P(B|A) \* P(A) / P(B)
- K Nearest Neighbour
- Linear Regression
- K Means Clustering


Feature selection:

- Set of all features
- Selecting the best subset: generate the subset = Learning Algorithm + Preformance



### section06 ###

Outline
- Metrics of validation
- Overfit vs Underfit
- Methods for improving accuracy
- Hyperparameter tuning

Confusion metric:

|    		  |    			  |    Actual   	|  		  Actual    |
| ----------- | ------------- | --------------- | ----------------- |
|    	-	  |    		-	  |   Positive		|  Negative         |
| Perdicted	  |	Positive	  | True positive 	| False Positive    |
| Preficted   | Negative	  | False Negative  | True Negative     |


Accuracy Score:

- Accuracy matters

Understanding precision:

Understanding Recal:

- Recognigtion
- Recall

Relationship amongs metrics

- TP = true positive
- TN = true negative
- FP = false positive
- FN = false negative

precision = TP / TP + FP

Recall = T? / TP + FN

F1 = 2 \* presision \* recall / resision + recall

RMSE Error:

RMSE = sqrt(sum N to i=1 (predicted_i - Actual_i)^2 / N)

 
###3 Overfit vs Underfit ####

Generalization problem in classification

uderfit = directline

good = smooth curve

overfit = mees line

#### Methods to imporove the accuracy ####

- add more data
- Feature Engineering
- Try different algorithm
- Feature selection
- Ensembling
- Model tuning

#### What are hyperparameter? ####

| Paramenter | Hyperparameter |
| ---------------------- | ---------------------------------- |
| The model parameter. 	 | Typically parameter of the algorithm used to optimize the ML model. |
| Also referred to as feature weight in ML and coefficient in algebra | Iser to contain the ML model (i.e. solve overfitting). |
| Part of model. | External to the model |
| Computed for the given dataset during training process | Independent of the given dataset and can't be estemated from data |
| Saved with the traind model | Not part of the trained or final model |

## Section 07 ##

Outline

- Why ensembling is required
- Different methods and techniques
- Ensembling in action


#### Bagging ####

Original Training data

- Step1: Create multiple data sets
- Step2: Build multible classifiers
- Step3: Combine Classifiers

#### Boosting ####

- weak learner 1, weak learner 2,  weak learner 3, and weak learner n  =  strong learner

#### Stacking ####

multible classification or/and regression model and second-leve algorithm = new predictions

## Section 08 ##

outline:

- High dimensional data good or bad?
- Methods to reduce dimensionality


## Section 09 ##

Some interesting trsources for learning see more: https://github.com/Geek-ubaid/Machine-Learning-Make-computers-think/blob/master/README.md